// console.log("Hello World!")


// What are the conditional statements?
	// Conditional Statements allows us to control the flow of our program. It allows us to run a statement/instruction if the condition is met or run another separate instruction if otherwise.

// [SECTION] if, else if, and else statement

let numA = -1;

	// if Statement
		// Execites a statement if the specified condition is true.
	if(numA < 0) {
		console.log("Hello");
	};

	/*
		Syntax:
			if(condition/s){
				statement/instruction
			}
	
	*/

	// You can also check the condition if it's true or false by printing the result in the console.
	console.log(numA < 0);
	// re-assign the value of numA with 0.
	numA = 0;
	// The if statement did not execute because the condition was not met.
	if(numA <0){
		console.log("Hello again if numA is less than 0: ")
	}
	// The condition results into false.
	console.log(numA < 0);

	// We can also use string data type to make coditional statements.
	let city = "New York";

	if(city === "New York");{
		console.log("Welcome to New York City");	
	}

	console.log (city === "New York")

	// else if Clause

	/*
		- Executes a statement if the previous conditions are false and if the specified condition is true.
		- The "else if" clause is optional and can be added to capture additional conditions to change the flow of the program.
	*/


	let numB = 1;
	if(numA < 0){
		console.log("Hello");
	}

	else if(numB >0) {
		console.log("World");
	}


	// If the if() condition was passed/met and run, the program will no longer evaluate the else if() condition and the process will stop.
	numA =1;

	if(numA>0){
		console.log("Hello");
	} else if (numB > 0){
		console.log("World");
	}



	// Let's update the city variable and make an if() and else if conditions:
	city= "Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York City! ");
	} else if(city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan!")
	}

	// else Statement

		/*

			-Executes a statement if all other conditions are false.
			- The "else" statement is also optional and can be added to capture any other result to change the flow of the program.
		*/

		if(numA === 0){ // the condition is false
			console.log("Hello");
		}else if(numB === 0){ // the condition is false
			console.log("World");
		}else{
			console.log("Again");
		}

		//else if and else are not stand alone statements.

		// // This will result to an error.
		// else if(numB === 0){ 
		// 	console.log("World");
		
		// // This will also result to an error.
		// }else{
		// 	console.log("Again");
		// }

		//if, else if, and else statement with functions.

		/*
			- Most of the times we would to if, else if and else statements with functions to control the flow of our application.

			-By including them inside functions, we can decide when a certain conditions will be checked instead of executing statements when JS loads.
		
		*/


		let message = "No message.";
		console.log(message);

		function determineTyphoonIntensity(windSpeed){

			// conditions

			if(windSpeed < 30){
				return "Not a typhoon yet.";
			} else if(windSpeed >=31 && windSpeed <= 61){
				return "Tropical depression detected";
			} else if(windSpeed >= 62 && windSpeed <= 88){
				return "Tropical storm detected"
			} else if(windSpeed >= 89 && windSpeed <= 177){
				return "Severe Tropical storm detected"

			} else{
				return "Typhoon detected.";
			}
		}


		message=determineTyphoonIntensity(32);	
		console.warn(message);




// [SECTION] Truthy and Falsy

		/*
			- In JS a "truthy" value is a value that is considered true when encountered in a Boolean context.
			- Value are considered true unless defined otherwise.
			- Falsy values:
				1. false
				2. 0
				3. ""
				4. null
				5. -0
				6. undefined
				7. NaN.
		*/

// Truthy Examples

	if(true){
		console.log("Truthy");
	}
	if(1){
		console.log("Truthy");
	}
	if([]){
		console.log("Truthy");
	}
	// let name ="France";

	// if(name) {
	// 	console.log("Truthy")
	// }


	// Falsy Examples

	if(false) {
		console.log("Falsy");

	}

	if(0){
		console.log("Falsy")
	};
	if(undefined){
		console.log("Falsy");
	}
	if(null){
		console.log("Falsy")
	}

	if(""){
		console.log("Falsy")
	}

	if(0){
		console.log("Falsy");
	}

	if(NaN){
		console.log("Falsy");
	}

//[SECTION] Conditional (Ternary) Operator

	/*
		Syntax
			(expression) ? ifTrue/Truthy : ifFalse/Falsy;
	*/

	// Single Statement Execution
	let ternaryResult =(1 < 18) ? true : false;
	console.log("Result of ternary Operator " + ternaryResult)

	// Multiple statement execution

	let name;

	function isOfLegalAge(){
		name ="john";
		return "You are of the legal age limit";

	}

	function isUnderAge(){
		name="Jane"
		return "You are under the age limit."
	}

	// let age = parseInt(prompt("What is your age"))
	// console.log(age)

	// let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
	// console.log("Result of Ternary Operator in functions:" + legalAge + ", " + name)


	// [SECTION] Switch Statement

	/*
		- the switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with the selected/matching case.

		Syntax:
			switch(expression){
				case value:
					statement;
					break;
				default:
					statement;
					break;
			};

	*/

	// // let day = prompt("What day of the week is it today?").toLowerCase();

	// // console.log(day);

	// switch(day) {
	// 	case 'monday' :
	// 		console.log("The color of the day is red");
	// 		break;
	// 	case 'tuesday' :
	// 		console.log("The color of the day is orange");
	// 		break;
	// 	case 'wednesday' :
	// 		console.log("The color of the day is yellow");
	// 		break;
	// 	case 'thursday' :
	// 		console.log("The color of the day is green");
	// 		break;
	// 	case 'friday' :
	// 		console.log("The color of the day is blue");
	// 		break;
	// 	case 'saturday' :
	// 		console.log("The color of the day is indigo");
	// 		break;
	// 	case 'sunday' :
	// 		console.log("The color of the day is violet");
	// 		break;
	// 	default:
	// 		console.log("Please input a valid day");
	// 		break;
	// }


//[SECTION] Try-Catch-Finally Statement

	function showIntensityAlert(windSpeed){

		// try-catch-finally statement

		try{
			// alerat(determineTyphoonIntensity(windSpeed)); - this will result to an error
			alert(determineTyphoonIntensity(windSpeed));
			
		}catch (error){
			console.log(typeof error);
			console.warn(error.message);
		} finally{
			alert("Intensity updates will show new alerts.");
		}

	}

	showIntensityAlert(56);

	console.log("This message will still show since we handled our error properly.")